<?php

class BooksController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /books
	 *
	 * @return Response
	 */
	public function index()
	{
		$public = Book::whereVisible(1)->take(10)->get();
		$follow = Book::following(Auth::user()->id)->get();

		//
		return View::make('books.index', [
			'public' => $public,
			'follow' => $follow,
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /books/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		return View::make('books.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /books
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /books/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		return View::make('books.show', ['book' => Book::find($id)]);
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /books/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /books/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /books/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
