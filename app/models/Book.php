<?php

class Book extends \Eloquent {
	protected $fillable = [];

	public function scopeFollowing($query, $user)
	{
		return $query->join('book_user', 'books.id',  '=', 'book_user.book_id')
			->where('book_user.user_id', $user);
	}

	
}
