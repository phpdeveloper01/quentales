@extends('layout')
@section('content')
    <h1>Recently Added</h1>
    <div id="recent">
    @foreach ($public as $pub)
	<div class="panel radius book-grid">
		<h3 class="booktitle">{{ HTML::linkRoute('books.show', $pub->title, [$pub->id]) }}</h3>
		{{ HTML::image($pub->image_url) }}
	</div>
    @endforeach
    </div>
    <h1>Following</h1>
    <div id="follow">
    @foreach ($follow as $pub)
	<div class="panel radius">
		<h3 class="booktitle">{{ HTML::linkRoute('books.show', $pub->title, [$pub->id]) }}</h3>
	</div>
    @endforeach
    </div>
@stop
