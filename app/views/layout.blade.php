<!DOCTYPE html>
<!--[if IE 9]<html class="lt-ie10" lang="en"> <![endif]-->
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Laravel Project</title>
    {{ HTML::style('css/foundation.css') }}
    {{ HTML::style('css/normalize.css') }}
    {{ HTML::style('css/style.css') }}

    <script src="{{ URL::asset('js/vendor/modernizr.js') }}"></script>
</head>
<body>
    <div id="container">
	<div class="fixed">
	<nav class="top-bar" data-topbar role="navigation">
            <ul class="title-area">
                <li class="name"><h1>{{ HTML::linkRoute('home', 'Quentale') }}</h1></li>
		<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
            </ul>

		 <section class="top-bar-section">
		<ul class="right">
                @if(Auth::check())
                    <li>{{ HTML::linkRoute('profile', 'My Profile' ) }}</li>
                    <li>{{ HTML::linkRoute('logout', 'Logout ('.Auth::user()->username.')') }}</li>
                @else
                    <li>{{ HTML::linkRoute('login', 'Login') }}</li>
                @endif
		</ul>
		</section>
	</nav>
        </div><!-- end nav -->

        <!-- check for flash notification message -->
        @if(Session::has('flash_notice'))
            <div id="flash_notice">{{ Session::get('flash_notice') }}</div>
        @endif

        @yield('content')
    </div><!-- end container -->
    <script src="{{ URL::asset('js/vendor/jquery.js') }}"></script>
  <script src="{{ URL::asset('js/foundation.min.js') }}"></script>
  <script src="{{ URL::asset('js/masonry.min.js') }}"></script>
  <script>
	$(document).foundation();
	var msnry = new Masonry( '#recent', {
		'columnWidth': 200, 
		'itemSelector': '.panel',
                'gutter' : 10
	});

  </script>
</body>
