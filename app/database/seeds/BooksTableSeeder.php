<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class BooksTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 100) as $index)
		{
			Book::create([
				'title' => $faker->sentence(7),
				'description' => $faker->paragraph(3), 
				'visible' => $faker->numberBetween(0,1),
				'image_url' => $faker->imageurl($faker->numberBetween(100,400), $faker->numberBetween(90, 600), 'cats'), 

			]);
		}
	}

}
